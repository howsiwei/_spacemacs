;; -*- mode: emacs-lisp -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Layer configuration:
This function should only modify configuration layer settings."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs

   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused

   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t

   ;; If non-nil layers with lazy install support are lazy installed.
   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()

   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   '(;; ----------------------------------------------------------------
     ;; Example of useful layers you may want to use right away.
     ;; Uncomment some layer names and press `SPC f e R' (Vim style) or
     ;; `M-m f e R' (Emacs style) to install them.
     ;; ----------------------------------------------------------------
     helm
     auto-completion
     templates
     better-defaults
     emacs-lisp
     csv
     epub
     markdown
     neotree
     (org :variables
          org-pretty-entities t)
     (shell :variables
            shell-default-shell 'shell
            shell-default-height 30
            shell-default-position 'bottom)
     ;; spell-checking
     syntax-checking
     (version-control :variables
                      version-control-diff-tool 'git-gutter
                      version-control-diff-side 'left)
     git
     ;; (multiple-cursors :variables multiple-cursors-backend 'evil-mc)
     vinegar
     xclipboard
     html
     (latex :variables
            latex-enable-magic t)
     yaml
     (c-c++ :variables
            c-c++-backend 'lsp-clangd
            c-c++-default-mode-for-headers 'c++-mode)
     (cmake :variables
            cmake-tab-width 4
            cmake-ide-rdm-rc-path "~/.config/rdm/rdm.conf"
            cmake-enable-cmake-ide-support t)
     (java :variables
           java-backend 'lsp-java)
     kotlin
     javascript
     csharp
     lsp
     dap
     (haskell :variables
              haskell-completion-backend 'lsp
              ;; haskell-process-type 'stack-ghci
      haskell-process-args-stack-ghci '("--ghci-options=-ferror-spans -fshow-loaded-modules")
      haskell-enable-hindent t
      )
     ocaml
     fsharp
     agda
     coq
     idris
     purescript
     (elixir :variables
             elixir-ls-path "~/repos/elixir-ls/release"
             elixir-backend 'lsp)
     (python :variables
             python-backend 'lsp)
     (ruby :variables
           ruby-version-manager 'chruby)
     rust
     nim
     lua
     go
     graphviz
     shell-scripts
     sql
     vimscript
     osx
     nixos
     protobuf
     docker
     nginx
     )

   ;; List of additional packages that will be installed without being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages, then consider creating a layer. You can also put the
   ;; configuration in `dotspacemacs/user-config'.
   ;; To use a local version of a package, use the `:location' property:
   ;; '(your-package :location "~/path/to/your-package/")
   ;; Also include the dependencies as they will not be resolved automatically.
   dotspacemacs-additional-packages
   '(
     rmsbolt
     persistent-scratch
     cryptol-mode
     )

   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '()

   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages
   '(
     ;; exec-path-from-shell
     )

   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and deletes any unused
   ;; packages as well as their unused dependencies. `used-but-keep-unused'
   ;; installs only the used packages but won't delete unused ones. `all'
   ;; installs *all* packages supported by Spacemacs and never uninstalls them.
   ;; (default is `used-only')
   dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization:
This function is called at the very beginning of Spacemacs startup,
before layer configuration.
It should only modify the values of Spacemacs settings."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non-nil then enable support for the portable dumper. You'll need
   ;; to compile Emacs 27 from source following the instructions in file
   ;; EXPERIMENTAL.org at to root of the git repository.
   ;; (default nil)
   dotspacemacs-enable-emacs-pdumper nil

   ;; File path pointing to emacs 27.1 executable compiled with support
   ;; for the portable dumper (this is currently the branch pdumper).
   ;; (default "emacs-27.0.50")
   dotspacemacs-emacs-pdumper-executable-file "emacs-27.0.50"

   ;; Name of the Spacemacs dump file. This is the file will be created by the
   ;; portable dumper in the cache directory under dumps sub-directory.
   ;; To load it when starting Emacs add the parameter `--dump-file'
   ;; when invoking Emacs 27.1 executable on the command line, for instance:
   ;;   ./emacs --dump-file=~/.emacs.d/.cache/dumps/spacemacs.pdmp
   ;; (default spacemacs.pdmp)
   dotspacemacs-emacs-dumper-dump-file "spacemacs.pdmp"

   ;; If non-nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t

   ;; Maximum allowed time in seconds to contact an ELPA repository.
   ;; (default 5)
   dotspacemacs-elpa-timeout 5

   ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
   ;; This is an advanced option and should not be changed unless you suspect
   ;; performance issues due to garbage collection operations.
   ;; (default '(100000000 0.1))
   dotspacemacs-gc-cons '(100000000 0.1)

   ;; If non-nil then Spacelpa repository is the primary source to install
   ;; a locked version of packages. If nil then Spacemacs will install the
   ;; latest version of packages from MELPA. (default nil)
   dotspacemacs-use-spacelpa nil

   ;; If non-nil then verify the signature for downloaded Spacelpa archives.
   ;; (default nil)
   dotspacemacs-verify-spacelpa-archives nil

   ;; If non-nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil

   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'. (default 'emacs-version)
   dotspacemacs-elpa-subdirectory 'emacs-version

   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'vim

   ;; If non-nil output loading progress in `*Messages*' buffer. (default nil)
   dotspacemacs-verbose-loading nil

   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'official

   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `bookmarks' `projects' `agenda' `todos'.
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   dotspacemacs-startup-lists '((recents . 5)
                                (projects . 7))

   ;; True if the home buffer should respond to resize events. (default t)
   dotspacemacs-startup-buffer-responsive t

   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'text-mode

   ;; Initial message in the scratch buffer, such as "Welcome to Spacemacs!"
   ;; (default nil)
   dotspacemacs-initial-scratch-message nil

   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press `SPC T n' to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(solarized-light
                         solarized-dark)

   ;; Set the theme for the Spaceline. Supported themes are `spacemacs',
   ;; `all-the-icons', `custom', `vim-powerline' and `vanilla'. The first three
   ;; are spaceline themes. `vanilla' is default Emacs mode-line. `custom' is a
   ;; user defined themes, refer to the DOCUMENTATION.org for more info on how
   ;; to create your own spaceline theme. Value can be a symbol or list with\
   ;; additional properties.
   ;; (default '(spacemacs :separator wave :separator-scale 1.5))
   dotspacemacs-mode-line-theme '(spacemacs :separator wave :separator-scale 1.5)

   ;; If non-nil the cursor color matches the state color in GUI Emacs.
   ;; (default t)
   dotspacemacs-colorize-cursor-according-to-state t

   ;; Default font, or prioritized list of fonts. `powerline-scale' allows to
   ;; quickly tweak the mode-line size to make separators look not too crappy.
   dotspacemacs-default-font '("Menlo"
                               :size 12
                               :weight normal
                               :width normal)

   ;; The leader key (default "SPC")
   dotspacemacs-leader-key "SPC"

   ;; The key used for Emacs commands `M-x' (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"

   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"

   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"

   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","

   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m")
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"

   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
   ;; Setting it to a non-nil value, allows for separate commands under `C-i'
   ;; and TAB or `C-m' and `RET'.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab t

   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"

   ;; If non-nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil

   ;; If non-nil then the last auto saved layouts are resumed automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil

   ;; If non-nil, auto-generate layout name when creating new layouts. Only has
   ;; effect when using the "jump to layout by number" commands. (default nil)
   dotspacemacs-auto-generate-layout-names nil

   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1

   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache

   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5

   ;; If non-nil, the paste transient-state is enabled. While enabled, pressing
   ;; `p' several times cycles through the elements in the `kill-ring'.
   ;; (default nil)
   dotspacemacs-enable-paste-transient-state nil

   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4

   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom

   ;; Control where `switch-to-buffer' displays the buffer. If nil,
   ;; `switch-to-buffer' displays the buffer in the current window even if
   ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
   ;; displays the buffer in a same-purpose window even if the buffer can be
   ;; displayed in the current window. (default nil)
   dotspacemacs-switch-to-buffer-prefers-purpose nil

   ;; If non-nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t

   ;; If non-nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil

   ;; If non-nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil

   ;; If non-nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup t

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90

   ;; If non-nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t

   ;; If non-nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t

   ;; If non-nil unicode symbols are displayed in the mode line.
   ;; If you use Emacs as a daemon and wants unicode characters only in GUI set
   ;; the value to quoted `display-graphic-p'. (default t)
   dotspacemacs-mode-line-unicode-symbols t

   ;; If non-nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t

   ;; Control line numbers activation.
   ;; If set to `t' or `relative' line numbers are turned on in all `prog-mode' and
   ;; `text-mode' derivatives. If set to `relative', line numbers are relative.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; (default nil)
   dotspacemacs-line-numbers t

   ;; Code folding method. Possible values are `evil' and `origami'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil

   ;; If non-nil `smartparens-strict-mode' will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil

   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc…
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil

   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all

   ;; If non-nil, start an Emacs server if one is not already running.
   ;; (default nil)
   dotspacemacs-enable-server nil

   ;; Set the emacs server socket location.
   ;; If nil, uses whatever the Emacs default is, otherwise a directory path
   ;; like \"~/.emacs.d/server\". It has no effect if
   ;; `dotspacemacs-enable-server' is nil.
   ;; (default nil)
   dotspacemacs-server-socket-dir nil

   ;; If non-nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil

   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
   ;; (default '("rg" "ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("rg" "ag" "pt" "ack" "grep")

   ;; Format specification for setting the frame title.
   ;; %a - the `abbreviated-file-name', or `buffer-name'
   ;; %t - `projectile-project-name'
   ;; %I - `invocation-name'
   ;; %S - `system-name'
   ;; %U - contents of $USER
   ;; %b - buffer name
   ;; %f - visited file name
   ;; %F - frame name
   ;; %s - process status
   ;; %p - percent of buffer above top of window, or Top, Bot or All
   ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
   ;; %m - mode name
   ;; %n - Narrow if appropriate
   ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
   ;; %Z - like %z, but including the end-of-line format
   ;; (default "%I@%S")
   dotspacemacs-frame-title-format "%I@%S"

   ;; Format specification for setting the icon title format
   ;; (default nil - same as frame-title-format)
   dotspacemacs-icon-title-format nil

   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed' to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup nil

   ;; Either nil or a number of seconds. If non-nil zone out after the specified
   ;; number of seconds. (default nil)
   dotspacemacs-zone-out-when-idle nil

   ;; Run `spacemacs/prettify-org-buffer' when
   ;; visiting README.org files of Spacemacs.
   ;; (default nil)
   dotspacemacs-pretty-docs t))

(defun dotspacemacs/user-env ()
  "Environment variables setup.
This function defines the environment variables for your Emacs session. By
default it calls `spacemacs/load-spacemacs-env' which loads the environment
variables declared in `~/.spacemacs.env' or `~/.spacemacs.d/.spacemacs.env'.
See the header of this file for more information."
  (spacemacs/load-spacemacs-env))

(defun dotspacemacs/user-init ()
  "Initialization for user code:
This function is called immediately after `dotspacemacs/init', before layer
configuration.
It is mostly for variables that should be set before packages are loaded.
If you are unsure, try setting them in `dotspacemacs/user-config' first."
  (setq evil-want-abbrev-expand-on-insert-exit nil)
  ;; (setq projectile-enable-caching t)
  ;; (setq projectile-indexing-method 'native)
  )

(defun dotspacemacs/user-load ()
  "Library to load while dumping.
This function is called only while dumping Spacemacs configuration. You can
`require' or `load' the libraries of your choice that will be included in the
dump."
  )

(defun dotspacemacs/user-config ()
  "Configuration for user code:
This function is called at the very end of Spacemacs startup, after layer
configuration.
Put your configuration code here, except for variables that should be set
before packages are loaded."
  (add-to-list 'default-frame-alist '(ns-appearance . dark))
  (setq-default warning-minimum-level :error)
  (setq shell-file-name "zsh")

  ; TODO try exclude path-helper
  (delete "/usr/local/bin" exec-path)
  (push "/usr/local/bin" exec-path)
  (setenv "PATH" "/usr/local/Cellar/zplug/2.4.2/bin:/usr/local/opt/zplug/bin:/Users/hsw/bin:/Users/hsw/sbin:/Users/hsw/.opam/default/bin:/usr/local/miniconda3/bin:/usr/local/miniconda3/condabin:/Users/hsw/.local/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/TeX/texbin:/opt/X11/bin:/Applications/Wireshark.app/Contents/MacOS")

  (setq evil-escape-delay 0.2)
  (setq evil-move-cursor-back nil)
  (setq evil-move-beyond-eol t)
  (setq evil-ex-substitute-global 1)
  (global-visual-line-mode t)

  (defun smart-beginning-of-line ()
    (interactive)
    (let ((oldpos (point)))
      (back-to-indentation)
      (and (= oldpos (point))
           (beginning-of-line))))

  (defun smart-backward-kill-line ()
    (interactive)
    (let ((oldpos (point)))
      (back-to-indentation)
      (if (= oldpos (point))
          (kill-line 0)
        (kill-region (point) oldpos))))

  ;; "\C-`" does not work
  (define-key global-map (kbd "C-`") 'help-command)
  (define-key global-map (kbd "H-e") #'evil-escape)
  (define-key global-map (kbd "H-i") #'evil-escape)
  (define-key global-map "\C-a" 'mwim-beginning-of-code-or-line)
  (define-key global-map "\C-e" 'mwim-end-of-line-or-code)
  (define-key global-map "\C-u" 'smart-backward-kill-line)
  (define-key global-map "\C-w" 'evil-delete-backward-word)

  (define-key evil-insert-state-map "\C-a" nil)
  (define-key evil-insert-state-map "\C-e" nil)
  (define-key evil-motion-state-map "\C-e" nil)

  (define-key evil-normal-state-map "\M-." nil)
  (define-key evil-normal-state-map "\C-j" 'evil-scroll-line-down)
  (define-key evil-normal-state-map "\C-k" 'evil-scroll-line-up)

  (define-key evil-normal-state-map "s" 'save-buffer)

  (spacemacs/set-leader-keys ":" #'pp-eval-expression)
  (spacemacs/set-leader-keys "xx" 'osx-dictionary-search-pointer)
  (spacemacs/set-leader-keys "ps" 'projectile-save-project-buffers)

  (with-eval-after-load 'helm
    (define-key helm-map "\C-w" 'nil))

  (with-eval-after-load 'helm-files
    (define-key helm-find-files-map "\C-w" 'helm-find-files-up-one-level))

  (setq org-agenda-files '("~/org/agenda"))

  (setq asm-comment-char ?\#)
  (setq c-default-style "stroustrup")
  ;; (add-to-list 'c-offsets-alist '(innamespace . 0))
  ;; (add-to-list 'c-offsets-alist '(inlambda . 0))
  (setq-default tab-width 4)
  (setq css-indent-offset 2)
  (setq js-indent-level 2)
  (setq kotlin-tab-width 4)
  (setq fish-indent-offset 4)
  (setq python-indent-offset 4)

  (setq sp-escape-quotes-after-insert nil) ; workaround for typing ' in c mode

  (setq python-shell-exec-path '("/usr/local/miniconda3/bin"))
  (setq python-shell-virtualenv-path "/usr/local/miniconda3/envs")

  (defun haskell-interactive-mode-backward-kill-line ()
    (interactive)
    (kill-region haskell-interactive-mode-prompt-start
                 (point)))

  (spacemacs/set-leader-keys-for-major-mode 'text-mode
    "," #'osx-dictionary-search-pointer
    )

  (with-eval-after-load 'haskell-interactive-mode
    (define-key haskell-interactive-mode-map "\C-u"
      'haskell-interactive-mode-backward-kill-line)
    )

  (setq lsp-ui-doc-enable nil)
  (setq lsp-ui-sideline-enable nil)
  (setq lsp-haskell-process-path-hie "hie-wrapper")

  (setq flycheck-gcc-definitions '("LOCAL"))
  (setq flycheck-c/c++-gcc-executable "/usr/local/bin/gcc-9")
  (setq flycheck-gcc-language-standard "c++17")
  (setq flycheck-clang-language-standard "c++17")
  ;; (add-hook
  ;;  'c++-mode-hook
  ;;  (lambda ()
  ;;    (setq tab-width 4)
  ;;    ;; (setq flycheck-checker 'c/c++-gcc)
  ;;    ))

  (defun my-nov-font-setup ()
    (face-remap-add-relative 'variable-pitch
                             :family "Liberation Serif"
                             :height 1.25))
  (add-hook 'nov-mode-hook 'my-nov-font-setup)

  (spacemacs/set-leader-keys-for-major-mode 'nov-mode
    "," #'osx-dictionary-search-pointer
    )



  (spacemacs/set-leader-keys-for-major-mode 'org-mode
    "eb" #'org-beamer-export-to-pdf
    "el" #'org-latex-export-to-pdf
    )

  (with-eval-after-load 'org
    (delete* "grffile" org-latex-default-packages-alist
             :test 'equal
             :key (lambda (e) (car (cdr e))))
    )

  (spacemacs/set-leader-keys-for-major-mode 'rust-mode
    "," #'cargo-process-check
    )

  (with-eval-after-load 'proof-site
    (spacemacs/set-leader-keys-for-major-mode 'coq-mode
      "[" nil
      "," 'proof-assert-next-command-interactive
      "]" nil
      "/" 'proof-undo-last-successful-command
      )

    (defvar coq-common-mode-map (make-sparse-keymap))

    (define-minor-mode coq-common-mode
      ""
      :init-value nil
      :keymap coq-common-mode-map)

    (add-hook 'coq-mode-hook #'coq-common-mode)
    (add-hook 'coq-goals-mode-hook #'coq-common-mode)
    (add-hook 'coq-response-mode-hook #'coq-common-mode)

    (add-hook 'coq-common-mode-hook #'rainbow-delimiters-mode)

    (spacemacs/set-leader-keys-for-minor-mode 'coq-common-mode
      ;; Layout
      "lc" 'pg-response-clear-displays
      "ll" 'proof-layout-windows
      "lp" 'proof-prf
      ;; Prover Interaction
      "pi" 'proof-interrupt-process
      "pq" 'proof-shell-exit
      ;; Prover queries ('ask prover')
      "aa" 'coq-Print
      "aA" 'coq-Print-with-all
      "ab" 'coq-About
      "aB" 'coq-About-with-all
      "ac" 'coq-Check
      "aC" 'coq-Check-show-all
      "af" 'proof-find-theorems
      "aib" 'coq-About-with-implicits
      "aic" 'coq-Check-show-implicits
      "aii" 'coq-Print-with-implicits))

  (with-eval-after-load 'company-coq
    (spacemacs/set-leader-keys-for-minor-mode 'coq-common-mode
      "ao" 'company-coq-occur
      "he" 'company-coq-document-error
      "hE" 'company-coq-browse-error-messages
      "hh" 'company-coq-doc)
    (put 'company-coq-fold 'disabled nil)
    )

  (setq persistent-scratch-autosave-interval 10)
  (persistent-scratch-setup-default)
  )

;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
(defun dotspacemacs/emacs-custom-settings ()
  "Emacs custom settings.
This is an auto-generated function, do not modify its content directly, use
Emacs customize menu instead.
This function is called at the very end of Spacemacs initialization."
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(evil-want-Y-yank-to-eol nil)
 '(hl-todo-keyword-faces
   '(("TODO" . "#dc752f")
     ("NEXT" . "#dc752f")
     ("THEM" . "#2d9574")
     ("PROG" . "#4f97d7")
     ("OKAY" . "#4f97d7")
     ("DONT" . "#f2241f")
     ("FAIL" . "#f2241f")
     ("DONE" . "#86dc2f")
     ("NOTE" . "#b1951d")
     ("KLUDGE" . "#b1951d")
     ("HACK" . "#b1951d")
     ("TEMP" . "#b1951d")
     ("FIXME" . "#dc752f")
     ("XXX+" . "#dc752f")
     ("\\?\\?\\?+" . "#dc752f")))
 '(package-selected-packages
   '(protobuf-mode xterm-color vterm shell-pop multi-term eshell-z eshell-prompt-extras esh-help nov esxml nim-mode flycheck-nimsuggest commenter flycheck-nim rmsbolt kotlin-mode flycheck-kotlin ob-elixir helm-gtags ggtags flycheck-mix flycheck-credo counsel-gtags alchemist elixir-mode mvn meghanada maven-test-mode lsp-java groovy-mode groovy-imports pcache gradle-mode ensime sbt-mode scala-mode company-emacs-eclim eclim symbol-overlay rubocopfmt org-cliplink dune devdocs cpp-auto-include company-reftex graphviz-dot-mode lsp-treemacs treemacs pfuture helm-lsp flycheck-package package-lint psci purescript-mode psc-ide add-node-modules-path omnisharp csharp-mode nginx-mode yasnippet-snippets web-mode solarized-theme racer proof-general persistent-scratch org-projectile org-download lsp-ui json-navigator hlint-refactor hl-todo helm-xref evil-visual-mark-mode evil-nerd-commenter evil-magit editorconfig doom-modeline diff-hl cryptol-mode cquery counsel-projectile counsel swiper ivy company-lsp aggressive-indent ace-window ace-link rust-mode lua-mode json-mode tablist smartparens merlin flycheck company projectile window-purpose imenu-list helm helm-core avy lsp-mode magit inf-ruby powerline caml dash visual-fill-column org-plus-contrib yatemplate yapfify yaml-mode ws-butler writeroom-mode winum which-key web-beautify volatile-highlights vimrc-mode vi-tilde-fringe uuidgen utop use-package unfill tuareg toml-mode toc-org tagedit symon string-inflection sql-indent spaceline-all-the-icons smeargle slim-mode shrink-path seeing-is-believing scss-mode sass-mode rvm ruby-tools ruby-test-mode ruby-refactor ruby-hash-syntax rubocop rspec-mode robe reveal-in-osx-finder restart-emacs rbenv rake rainbow-delimiters pyvenv pytest pyenv-mode py-isort pug-mode prettier-js popwin pippel pipenv pip-requirements persp-mode password-generator paradox overseer osx-trash osx-dictionary orgit org-present org-pomodoro org-mime org-category-capture org-bullets org-brain open-junk-file ocp-indent nix-mode neotree nameless mwim move-text mmm-mode minitest markdown-toc magit-svn magit-gitflow magic-latex-buffer macrostep lsp-haskell lsp-go lorem-ipsum livid-mode live-py-mode link-hint launchctl json-snatcher json-reformat js2-refactor js-doc insert-shebang indent-guide importmagic impatient-mode idris-mode hungry-delete ht hindent highlight-parentheses highlight-numbers highlight-indentation hierarchy helm-themes helm-swoop helm-rtags helm-pydoc helm-purpose helm-projectile helm-org-rifle helm-nixos-options helm-mode-manager helm-make helm-hoogle helm-gitignore helm-git-grep helm-flx helm-descbinds helm-ctest helm-css-scss helm-cscope helm-company helm-c-yasnippet helm-ag haskell-snippets google-translate google-c-style golden-ratio godoctor go-tag go-rename go-impl go-guru go-gen-test go-fill-struct go-eldoc gnuplot gitignore-templates gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link git-gutter-fringe git-gutter-fringe+ gh-md fuzzy fsharp-mode font-lock+ flycheck-rust flycheck-rtags flycheck-pos-tip flycheck-ocaml flycheck-haskell flycheck-bashate flx-ido fish-mode fill-column-indicator fancy-battery eyebrowse expand-region evil-visualstar evil-unimpaired evil-tutor evil-surround evil-org evil-numbers evil-matchit evil-lisp-state evil-lion evil-indent-plus evil-iedit-state evil-goggles evil-exchange evil-escape evil-ediff evil-cleverparens evil-args evil-anzu eval-sexp-fu emmet-mode elisp-slime-nav eldoc-eval dumb-jump dotenv-mode dockerfile-mode docker disaster diminish dactyl-mode cython-mode csv-mode company-web company-tern company-statistics company-shell company-rtags company-nixos-options company-lua company-go company-ghci company-coq company-cabal company-c-headers company-auctex company-anaconda column-enforce-mode cmm-mode cmake-mode cmake-ide clean-aindent-mode clang-format chruby centered-cursor-mode ccls cargo bundler browse-at-remote auto-yasnippet auto-highlight-symbol auto-compile auctex-latexmk ace-jump-helm-line ac-ispell))
 '(paradox-github-token t)
 '(pdf-view-midnight-colors '("#b2b2b2" . "#292b2e"))
 '(psc-ide-add-import-on-completion t t)
 '(psc-ide-rebuild-on-save nil t)
 '(safe-local-variable-values
   '((lsp-pyls-plugins-pycodestyle-enabled)
     (javascript-backend . tern)
     (javascript-backend . lsp)
     (go-backend . go-mode)
     (go-backend . lsp)
     (elixir-enable-compilation-checking . t)
     (elixir-enable-compilation-checking))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
)
